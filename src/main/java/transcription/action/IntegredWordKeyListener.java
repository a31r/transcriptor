/*
 * Decompiled with CFR 0_118.
 */
package transcription.action;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JTextPane;
import transcription.gui.MainForm;

public class IntegredWordKeyListener
extends KeyAdapter {
    MainForm mainForm;
    int isSupDelete;
    int isDelDelete;

    public IntegredWordKeyListener(MainForm mainForm) {
        this.mainForm = mainForm;
        this.isSupDelete = -1;
        this.isDelDelete = -1;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int caret;
        super.keyPressed(e);
        char i = e.getKeyChar();
        if (i == '') {
            caret = this.mainForm.getInterviewLog().getCaretPosition();
            if (this.mainForm.getInterviewLog().getText().charAt(caret) == '<') {
                this.isSupDelete = caret;
            }
            if (this.mainForm.getInterviewLog().getText().charAt(caret) == '>') {
                this.isSupDelete = -1;
            }
        } else {
            this.isSupDelete = -1;
        }
        if (i == '\b') {
            caret = this.mainForm.getInterviewLog().getCaretPosition();
            if (caret > 0) {
                if (this.mainForm.getInterviewLog().getText().charAt(caret - 1) == '>') {
                    this.isDelDelete = caret;
                }
                if (this.mainForm.getInterviewLog().getText().charAt(caret - 1) == '<') {
                    this.isDelDelete = -1;
                }
            } else {
                this.isDelDelete = -1;
            }
        } else {
            this.isDelDelete = -1;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int index;
        String text;
        super.keyReleased(e);
        if (this.isSupDelete != -1 && (index = (text = this.mainForm.getInterviewLog().getText()).indexOf(62, this.isSupDelete)) > 0) {
            this.mainForm.getInterviewLog().setText(text.substring(0, this.isSupDelete) + text.substring(index + 1));
            this.mainForm.getInterviewLog().setCaretPosition(this.isSupDelete);
        }
        if (this.isDelDelete > 0 && (index = (text = this.mainForm.getInterviewLog().getText()).lastIndexOf(60)) > 0) {
            this.mainForm.getInterviewLog().setText(text.substring(0, index) + text.substring(this.isDelDelete - 1));
            this.mainForm.getInterviewLog().setCaretPosition(index);
        }
        this.isDelDelete = -1;
        this.isSupDelete = -1;
    }
}

