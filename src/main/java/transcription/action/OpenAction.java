/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  javax.media.bean.playerbean.MediaPlayer
 */
package transcription.action;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.media.bean.playerbean.MediaPlayer;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.filechooser.FileFilter;
import transcription.Constants;
import transcription.Internationalization;
import transcription.gui.MainForm;
import transcription.transcription.TXTConverter;
import transcription.transcription.TranscriptionConverter;

public class OpenAction
implements ActionListener {
    MainForm mainForm;
    protected String LAST_FOLDER = null;

    public OpenAction(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    public MainForm getMainForm() {
        return this.mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            JFileChooser chooser = new JFileChooser();
            if (this.LAST_FOLDER != null) {
                chooser.setCurrentDirectory(new File(this.LAST_FOLDER));
            }
            chooser.addChoosableFileFilter(new FileFilter(){

                @Override
                public boolean accept(File f) {
                    if (f.isDirectory() || f.getAbsolutePath().toUpperCase().endsWith("RTF")) {
                        return true;
                    }
                    return false;
                }

                @Override
                public String getDescription() {
                    return "Transana RTF Format (UTF-8) Unicode 8-bit";
                }
            });
            chooser.addChoosableFileFilter(new FileFilter(){

                @Override
                public boolean accept(File f) {
                    if (f.isDirectory() || f.getAbsolutePath().toUpperCase().endsWith("RTF")) {
                        return true;
                    }
                    return false;
                }

                @Override
                public String getDescription() {
                    return "Transana RTF Format (UTF-16) Unicode 16-bit";
                }
            });
            chooser.addChoosableFileFilter(new FileFilter(){

                @Override
                public boolean accept(File f) {
                    if (f.isDirectory() || f.getAbsolutePath().toUpperCase().endsWith("TXT")) {
                        return true;
                    }
                    return false;
                }

                @Override
                public String getDescription() {
                    return "SACODEYL Transcriptor TXT Format (Cp1252) ANSI Windows";
                }
            });
            chooser.addChoosableFileFilter(new FileFilter(){

                @Override
                public boolean accept(File f) {
                    if (f.isDirectory() || f.getAbsolutePath().toUpperCase().endsWith("TXT")) {
                        return true;
                    }
                    return false;
                }

                @Override
                public String getDescription() {
                    return "SACODEYL Transcriptor TXT Format (ASCII) ASCII Code";
                }
            });
            chooser.addChoosableFileFilter(new FileFilter(){

                @Override
                public boolean accept(File f) {
                    if (f.isDirectory() || f.getAbsolutePath().toUpperCase().endsWith("TXT")) {
                        return true;
                    }
                    return false;
                }

                @Override
                public String getDescription() {
                    return "SACODEYL Transcriptor TXT Format (ISO8859_1) ISO Latin Charset";
                }
            });
            chooser.addChoosableFileFilter(new FileFilter(){

                @Override
                public boolean accept(File f) {
                    if (f.isDirectory() || f.getAbsolutePath().toUpperCase().endsWith("TXT")) {
                        return true;
                    }
                    return false;
                }

                @Override
                public String getDescription() {
                    return "SACODEYL Transcriptor TXT Format (UTF-8) Unicode 8-bit";
                }
            });
            chooser.addChoosableFileFilter(new FileFilter(){

                @Override
                public boolean accept(File f) {
                    if (f.isDirectory() || f.getAbsolutePath().toUpperCase().endsWith("TXT")) {
                        return true;
                    }
                    return false;
                }

                @Override
                public String getDescription() {
                    return "SACODEYL Transcriptor TXT Format (UTF-16) Unicode 16-bit";
                }
            });
            if (chooser.showOpenDialog(this.getMainForm().getMainPanel()) == 0) {
                String filePath = chooser.getSelectedFile().getAbsolutePath();
                this.LAST_FOLDER = chooser.getSelectedFile().getParent();
                Constants.SAVE_FILE = null;
                String ext = filePath.substring(filePath.lastIndexOf(46) + 1);
                ext = ext.toUpperCase();
                Properties prop = new Properties();
                prop.loadFromXML(new FileInputStream(Constants.FILE_CONVERSION));
                String className = prop.getProperty(ext);
                TranscriptionConverter tc = null;
                tc = className != null ? (TranscriptionConverter)Class.forName(className).newInstance() : new TXTConverter();
                Boolean hasEncoding = chooser.getFileFilter().getDescription().indexOf(")") != -1 && chooser.getFileFilter().getDescription().indexOf("(") != - 1;
                String encoding = hasEncoding ? chooser.getFileFilter().getDescription().substring(chooser.getFileFilter().getDescription().indexOf("(") + 1, chooser.getFileFilter().getDescription().indexOf(")")) : "UTF-16";
                this.getMainForm().getInterviewLog().setText(tc.getTranscription(chooser.getSelectedFile().getAbsolutePath(), encoding));
                this.getMainForm().getStatus().setText(Internationalization.STATUS_OPEN);
            }
        }
        catch (IOException e1) {
            JOptionPane.showMessageDialog((Component)this.getMainForm().getMediaPlayer(), Internationalization.OPEN_FILE_IOEXCEPTION);
        }
        catch (IllegalAccessException e1) {
            JOptionPane.showMessageDialog((Component)this.getMainForm().getMediaPlayer(), Internationalization.OPEN_FILE_ILLEGAL_ACCESS_EXCEPTION);
        }
        catch (ClassNotFoundException e1) {
            JOptionPane.showMessageDialog((Component)this.getMainForm().getMediaPlayer(), Internationalization.OPEN_FILE_CLASS_NOT_FOUND_EXCEPTION);
        }
        catch (InstantiationException e1) {
            JOptionPane.showMessageDialog((Component)this.getMainForm().getMediaPlayer(), Internationalization.OPEN_FILE_CLASS_NOT_FOUND_EXCEPTION);
        }
    }

}

