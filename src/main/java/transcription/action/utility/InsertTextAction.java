/*
 * Decompiled with CFR 0_118.
 */
package transcription.action.utility;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextPane;
import transcription.gui.MainForm;

public class InsertTextAction
implements ActionListener {
    MainForm mainForm;
    String tag;

    public InsertTextAction(MainForm mainForm, String tag) {
        this.mainForm = mainForm;
        this.tag = tag;
    }

    public MainForm getMainForm() {
        return this.mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int caretPos = this.getMainForm().getInterviewLog().getCaretPosition();
        String text = this.getMainForm().getInterviewLog().getText();
        this.getMainForm().getInterviewLog().setText(text.substring(0, caretPos) + this.tag + text.substring(caretPos));
        this.getMainForm().getInterviewLog().setCaretPosition(caretPos + this.tag.length());
        this.getMainForm().getInterviewLog().requestFocus();
    }
}

