/*
 * Decompiled with CFR 0_118.
 */
package transcription.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import transcription.gui.ConfigForm;
import transcription.gui.MainForm;

public class ShowConfigForm
implements ActionListener {
    MainForm mainForm;

    public ShowConfigForm(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ConfigForm form = new ConfigForm(this.mainForm);
    }
}

