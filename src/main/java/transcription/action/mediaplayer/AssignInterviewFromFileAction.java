/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  javax.media.bean.playerbean.MediaPlayer
 */
package transcription.action.mediaplayer;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.media.bean.playerbean.MediaPlayer;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import transcription.gui.MainForm;

public class AssignInterviewFromFileAction
implements ActionListener {
    MainForm mainForm;
    protected String LAST_FOLDER = null;

    public AssignInterviewFromFileAction(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    public MainForm getMainForm() {
        return this.mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser chooser = new JFileChooser();
        if (this.LAST_FOLDER != null) {
            chooser.setCurrentDirectory(new File(this.LAST_FOLDER));
        }
        FileFilter all = chooser.getFileFilter();
        chooser.addChoosableFileFilter(new FileNameExtensionFilter("Wave Audio WAV Format (*.wav, *.pcm)", "wav", "pcm"));
        chooser.addChoosableFileFilter(new FileNameExtensionFilter("MPEG Layer III Audio MP3 Format (*.mp3)", "mp3"));
        chooser.addChoosableFileFilter(new FileNameExtensionFilter("Windows Media Audio WMA Format (*.wma)", "wma"));
        chooser.addChoosableFileFilter(new FileNameExtensionFilter("Vorbis OGG Format (*.ogg)", "ogg"));
        chooser.addChoosableFileFilter(new FileNameExtensionFilter("Windows Media Video Format (*.wmv)", "wmv"));
        chooser.addChoosableFileFilter(new FileNameExtensionFilter("Real Media Format (*.rm)", "rm", "ram"));
        chooser.addChoosableFileFilter(new FileNameExtensionFilter("Quick Time Format (*.mov)", "mov", "qt"));
        chooser.addChoosableFileFilter(new FileNameExtensionFilter("MPEG-2 Video Format (*.mpg, *.mpeg)", "mpeg", "mpg"));
        chooser.addChoosableFileFilter(new FileNameExtensionFilter("OGM Video Format (*.ogm)", "ogm"));
        chooser.addChoosableFileFilter(new FileNameExtensionFilter("AVI Video Format (*.avi,*,divx,*.xvid,*.mp4)", "avi", "divx", "xvid", "mp4"));
        chooser.addChoosableFileFilter(all);
        if (0 == chooser.showOpenDialog(this.getMainForm().getMainPanel())) {
            this.LAST_FOLDER = chooser.getSelectedFile().getParent();
            String path = chooser.getSelectedFile().getAbsolutePath();
            this.getMainForm().getMediaPlayer().stop();
            this.getMainForm().getMediaPlayer().deallocate();
            this.getMainForm().getMediaPlayer().setMediaLocation("file:///" + path);
            this.getMainForm().getMediaPlayer().realize();
            this.getMainForm().getMediaPlayer().prefetch();
            this.getMainForm().getMediaPlayer().start();
            this.getMainForm().getMediaPlayer().waitForState(600);
            this.getMainForm().getMediaPlayer().setSize(this.getMainForm().getMediaPanel().getHeight(), this.getMainForm().getMediaPanel().getHeight());
            this.getMainForm().getSetEndButton().setEnabled(true);
            this.getMainForm().getSetStartButton().setEnabled(true);
            this.getMainForm().getSetIntermidiumButton().setEnabled(true);
            this.getMainForm().getMenuBar().getMenu(1).getMenuComponent(1).setEnabled(true);
            this.getMainForm().getMenuBar().getMenu(1).getMenuComponent(2).setEnabled(true);
            this.getMainForm().getMenuBar().getMenu(1).getMenuComponent(3).setEnabled(true);
        }
    }
}

