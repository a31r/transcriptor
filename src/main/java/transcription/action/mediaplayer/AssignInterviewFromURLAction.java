/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  javax.media.bean.playerbean.MediaPlayer
 */
package transcription.action.mediaplayer;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.media.bean.playerbean.MediaPlayer;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import transcription.Internationalization;
import transcription.gui.MainForm;

public class AssignInterviewFromURLAction
implements ActionListener {
    MainForm mainForm;

    public AssignInterviewFromURLAction(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    public MainForm getMainForm() {
        return this.mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String text = JOptionPane.showInputDialog(this.getMainForm().getMainPanel(), Internationalization.LOAD_URL_MESSAGE, Internationalization.LOAD_URL_TITLE, 1);
        if (text != null) {
            this.getMainForm().getMediaPlayer().stop();
            this.getMainForm().getMediaPlayer().setMediaLocation(text);
            this.getMainForm().getMediaPlayer().realize();
            this.getMainForm().getMediaPlayer().prefetch();
            this.getMainForm().getMediaPlayer().start();
            this.getMainForm().getSetEndButton().setEnabled(true);
            this.getMainForm().getSetStartButton().setEnabled(true);
            this.getMainForm().getSetIntermidiumButton().setEnabled(true);
            this.getMainForm().getMenuBar().getMenu(1).getMenuComponent(1).setEnabled(true);
            this.getMainForm().getMenuBar().getMenu(1).getMenuComponent(2).setEnabled(true);
            this.getMainForm().getMenuBar().getMenu(1).getMenuComponent(3).setEnabled(true);
        }
    }
}

