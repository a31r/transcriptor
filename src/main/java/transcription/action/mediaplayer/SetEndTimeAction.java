/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  javax.media.Time
 *  javax.media.bean.playerbean.MediaPlayer
 */
package transcription.action.mediaplayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import javax.media.Time;
import javax.media.bean.playerbean.MediaPlayer;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import transcription.Constants;
import transcription.Internationalization;
import transcription.gui.MainForm;

public class SetEndTimeAction
implements ActionListener {
    MainForm mainForm;

    public SetEndTimeAction(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    public MainForm getMainForm() {
        return this.mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        DecimalFormat nf = new DecimalFormat("00.00");
        String second = nf.format(this.getMainForm().getMediaPlayer().getMediaTime().getSeconds());
        int carret = this.getMainForm().getInterviewLog().getCaretPosition();
        int initDoc = this.getMainForm().getInterviewLog().getText().indexOf("[BODY]");
        initDoc = initDoc != -1 ? (initDoc += 6) : 0;
        int endLineCarret = carret + this.getMainForm().getInterviewLog().getText().substring(carret).indexOf("\n");
        int initLine = this.getMainForm().getInterviewLog().getText().substring(initDoc, endLineCarret).lastIndexOf("]");
        if (initLine == -1) {
            JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), Internationalization.START_TIME_ERROR);
        } else {
            initLine += initDoc;
            initLine = this.getMainForm().getInterviewLog().getText().substring(initDoc, initLine).lastIndexOf(10);
            initLine += initDoc;
            if (Constants.TIME_STAMPING_LEVEL.equalsIgnoreCase("U")) {
                if (this.getMainForm().getInterviewLog().getText().charAt(initLine + 1) == '[') {
                    initLine = this.getMainForm().getInterviewLog().getText().substring(0, carret).lastIndexOf("]");
                    this.getMainForm().getInterviewLog().setText(this.getMainForm().getInterviewLog().getText().substring(0, initLine + 1) + " [" + second + "] " + this.getMainForm().getInterviewLog().getText().substring(initLine + 1));
                    this.getMainForm().getInterviewLog().setCaretPosition(carret + second.length() + 4);
                } else {
                    JOptionPane.showMessageDialog(this.getMainForm().getInterviewLog(), Internationalization.SET_END_TIME_EXCEPTION);
                }
            } else if (Constants.TIME_STAMPING_LEVEL.equalsIgnoreCase("S")) {
                int endSection = this.getMainForm().getInterviewLog().getText().substring(carret).indexOf("#") - 1;
                endSection += carret;
                if (initLine > -1) {
                    initLine = endSection;
                    initLine = this.getMainForm().getInterviewLog().getText().substring(initDoc, initLine).lastIndexOf(10);
                    if (this.getMainForm().getInterviewLog().getText().substring(initLine += initDoc, endSection).indexOf("[") != -1) {
                        initLine = this.getMainForm().getInterviewLog().getText().substring(0, endSection).lastIndexOf("]");
                        this.getMainForm().getInterviewLog().setText(this.getMainForm().getInterviewLog().getText().substring(0, initLine + 1) + " [" + second + "] " + this.getMainForm().getInterviewLog().getText().substring(initLine + 1));
                        this.getMainForm().getInterviewLog().setCaretPosition(carret + second.length() + 3);
                    } else {
                        this.getMainForm().getInterviewLog().setText(this.getMainForm().getInterviewLog().getText().substring(0, initLine) + "\n[" + second + "] " + this.getMainForm().getInterviewLog().getText().substring(initLine + 1));
                        this.getMainForm().getInterviewLog().setCaretPosition(carret + second.length() + 3);
                    }
                } else {
                    JOptionPane.showMessageDialog(this.getMainForm().getInterviewLog(), Internationalization.SET_END_TIME_EXCEPTION_SEC);
                }
            }
            this.getMainForm().getInterviewLog().requestFocus();
        }
    }
}

