/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  javax.media.Time
 *  javax.media.bean.playerbean.MediaPlayer
 */
package transcription.action.mediaplayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import javax.media.Time;
import javax.media.bean.playerbean.MediaPlayer;
import javax.swing.JTextPane;
import transcription.gui.MainForm;

public class SetIntermediumTimeAction
implements ActionListener {
    MainForm mainForm;

    public MainForm getMainForm() {
        return this.mainForm;
    }

    public SetIntermediumTimeAction(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        DecimalFormat nf = new DecimalFormat("00.00");
        String second = nf.format(this.getMainForm().getMediaPlayer().getMediaTime().getSeconds());
        int carret = this.getMainForm().getInterviewLog().getCaretPosition();
        this.getMainForm().getInterviewLog().setText(this.getMainForm().getInterviewLog().getText().substring(0, carret) + "[" + second + "]" + this.getMainForm().getInterviewLog().getText().substring(carret));
        this.getMainForm().getInterviewLog().setCaretPosition(carret + second.length() + 3);
        this.getMainForm().getInterviewLog().requestFocus();
    }
}

