/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  javax.media.bean.playerbean.MediaPlayer
 */
package transcription.action;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;
import javax.media.bean.playerbean.MediaPlayer;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import transcription.Constants;
import transcription.Internationalization;
import transcription.gui.MainForm;

public class NuevoAction
implements ActionListener {
    MainForm mainForm;

    public MainForm getMainForm() {
        return this.mainForm;
    }

    public NuevoAction(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            File dir = new File(Constants.DIR_TEMPLATE);
            File[] files = dir.listFiles();
            Object[] names = new String[files.length];
            for (int i = 0; i < files.length; ++i) {
                names[i] = files[i].getName().substring(0, files[i].getName().lastIndexOf("."));
            }
            Object selected = JOptionPane.showInputDialog(this.mainForm.getMainPanel(), Internationalization.SELECT_TEMPLATE, Internationalization.SELECT_TEMPLATE_TITLE, 2, null, names, null);
            if (selected != null) {
                int index = Arrays.binarySearch(names, selected);
                FileReader fr = new FileReader(files[index]);
                String aux = null;
                BufferedReader br = new BufferedReader(fr);
                String total = "";
                while ((aux = br.readLine()) != null) {
                    total = total + aux + "\n";
                }
                fr.close();
                this.getMainForm().getInterviewLog().setText(total);
                this.getMainForm().getStatus().setText(Internationalization.STATUS_NEW);
                Constants.SAVE_FILE = null;
            }
        }
        catch (FileNotFoundException e1) {
            JOptionPane.showMessageDialog((Component)this.getMainForm().getMediaPlayer(), Internationalization.NEW_FILE_FILE_NOT_FOUND_EXCEPTION);
        }
        catch (IOException e1) {
            JOptionPane.showMessageDialog((Component)this.getMainForm().getMediaPlayer(), Internationalization.NEW_FILE_IOEXCEPTION);
        }
    }
}

