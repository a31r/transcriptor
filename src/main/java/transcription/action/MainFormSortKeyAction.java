/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  javax.media.Time
 *  javax.media.bean.playerbean.MediaPlayer
 */
package transcription.action;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.media.Time;
import javax.media.bean.playerbean.MediaPlayer;
import javax.swing.JLabel;
import transcription.Constants;
import transcription.Internationalization;
import transcription.action.GuardarAction;
import transcription.action.mediaplayer.SetEndTimeAction;
import transcription.action.mediaplayer.SetIntermediumTimeAction;
import transcription.action.mediaplayer.SetStartTimeAction;
import transcription.gui.MainForm;

public class MainFormSortKeyAction
implements KeyListener {
    MainForm mainForm;

    public MainFormSortKeyAction(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        this.mainForm.getStatus().setText("");
        if ((e.getModifiers() & 2) == 2) {
            Time newMediaTime;
            Time actualMediaTime;
            double d;
            if (e.getKeyCode() == 32) {
                if (this.mainForm.getMediaPlayer().getState() == 500) {
                    this.mainForm.getMediaPlayer().start();
                } else if (this.mainForm.getMediaPlayer().getState() == 600) {
                    this.mainForm.getMediaPlayer().stop();
                }
            }
            if (e.getKeyCode() == 83) {
                SetStartTimeAction s = new SetStartTimeAction(this.mainForm);
                s.actionPerformed(new ActionEvent(this, 1, "command"));
            }
            if (e.getKeyCode() == 68) {
                SetEndTimeAction en = new SetEndTimeAction(this.mainForm);
                en.actionPerformed(new ActionEvent(this, 2, "command2"));
            }
            if (e.getKeyCode() == 69) {
                SetIntermediumTimeAction ita = new SetIntermediumTimeAction(this.mainForm);
                ita.actionPerformed(new ActionEvent(this, 2, "command2"));
            }
            if (e.getKeyCode() == 82 && (actualMediaTime = this.mainForm.getMediaPlayer().getMediaTime()).getSeconds() - (double)Constants.REWIND_SECOND_SEEK >= 0.0) {
                d = actualMediaTime.getSeconds() - (double)Constants.REWIND_SECOND_SEEK;
                newMediaTime = new Time(d);
                this.mainForm.getMediaPlayer().setMediaTime(newMediaTime);
            }
            if (e.getKeyCode() == 70 && (actualMediaTime = this.mainForm.getMediaPlayer().getMediaTime()).getSeconds() - (double)Constants.REWIND_SECOND_SEEK >= 0.0) {
                d = actualMediaTime.getSeconds() + (double)Constants.REWIND_SECOND_SEEK;
                newMediaTime = new Time(d);
                this.mainForm.getMediaPlayer().setMediaTime(newMediaTime);
            }
            if (e.getKeyCode() == 81) {
                GuardarAction guardarAction = new GuardarAction(this.mainForm);
                if (Constants.SAVE_FILE != null) {
                    guardarAction.setApearSaveMenu(false);
                }
                guardarAction.actionPerformed(new ActionEvent(this, 1, "save"));
                this.mainForm.getStatus().setText(Internationalization.STATUS_SAVE);
            }
            if (e.getKeyCode() == 73) {
                this.mainForm.getMediaPlayer().setRate(this.mainForm.getMediaPlayer().getRate() + Constants.RATIO_INCREASING);
            }
            if (e.getKeyCode() == 85) {
                this.mainForm.getMediaPlayer().setRate(this.mainForm.getMediaPlayer().getRate() - Constants.RATIO_INCREASING);
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}

