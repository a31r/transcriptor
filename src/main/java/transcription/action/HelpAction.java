/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  javax.media.bean.playerbean.MediaPlayer
 */
package transcription.action;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.media.bean.playerbean.MediaPlayer;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.text.EditorKit;
import javax.swing.text.html.HTMLEditorKit;
import transcription.Constants;
import transcription.Internationalization;
import transcription.gui.HelpForm;
import transcription.gui.MainForm;

public class HelpAction
implements ActionListener {
    MainForm mainForm;

    public HelpAction(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    public MainForm getMainForm() {
        return this.mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFrame dialog = new JFrame("Help");
        HelpForm help = new HelpForm(dialog);
        help.getHelpPanel().setEditorKit(new HTMLEditorKit());
        try {
            help.getHelpPanel().setPage(Constants.FILE_HELP);
            dialog.setSize(800, 270);
            dialog.setLocationRelativeTo(this.getMainForm().getMainPanel());
            dialog.setVisible(true);
        }
        catch (IOException e1) {
            e1.printStackTrace();
            JOptionPane.showMessageDialog((Component)this.getMainForm().getMediaPlayer(), Internationalization.NEW_FILE_FILE_NOT_FOUND_EXCEPTION);
        }
    }
}

