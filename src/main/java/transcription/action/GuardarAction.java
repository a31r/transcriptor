/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  javax.media.bean.playerbean.MediaPlayer
 */
package transcription.action;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import javax.media.bean.playerbean.MediaPlayer;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import transcription.Constants;
import transcription.Internationalization;
import transcription.gui.MainForm;

public class GuardarAction
implements ActionListener {
    MainForm mainForm;
    boolean apearSaveMenu = true;
    protected String LAST_FOLDER = null;

    public MainForm getMainForm() {
        return this.mainForm;
    }

    public GuardarAction(MainForm mainForm) {
        this.mainForm = mainForm;
        this.apearSaveMenu = true;
    }

    public boolean isApearSaveMenu() {
        return this.apearSaveMenu;
    }

    public void setApearSaveMenu(boolean apearSaveMenu) {
        this.apearSaveMenu = apearSaveMenu;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String path = null;
        boolean cent = true;
        if (this.apearSaveMenu) {
            JFileChooser chooser = new JFileChooser();
            if (this.LAST_FOLDER != null) {
                chooser.setCurrentDirectory(new File(this.LAST_FOLDER));
            }
            cent = false;
            if (chooser.showSaveDialog(this.getMainForm().getMainPanel()) == 0) {
                cent = true;
                path = chooser.getSelectedFile().getAbsolutePath();
                this.LAST_FOLDER = chooser.getSelectedFile().getParent();
                if (!path.toUpperCase().contains(".TXT")) {
                    path = path + ".txt";
                }
                Constants.SAVE_FILE = path;
            }
        } else {
            path = Constants.SAVE_FILE;
        }
        try {
            if (cent) {
                String aux = this.getMainForm().getInterviewLog().getText();
                if (Constants.PERFORM_VALIDATION.equalsIgnoreCase("TRUE")) {
                    this.validateDocument(aux, this.getMainForm());
                }
                FileOutputStream fw = new FileOutputStream(path, false);
                OutputStreamWriter outp = new OutputStreamWriter((OutputStream)fw, Constants.ENCODING);
                outp.write(this.writeIt(aux));
                outp.flush();
                outp.close();
                fw.flush();
                fw.close();
                this.getMainForm().getStatus().setText(Internationalization.STATUS_SAVE);
            }
        }
        catch (IOException e1) {
            JOptionPane.showMessageDialog((Component)this.getMainForm().getMediaPlayer(), Internationalization.SAVE_FILE_IOEXCEPTION);
        }
    }

    public boolean validateDocument(String document, MainForm mainForm) {
        int numberOfDoubleCommas = 0;
        int numberOfOpenParentesis = 0;
        int numberOfCloseParentesis = 0;
        int numberOfOpenKey = 0;
        int numberOfCloseKey = 0;
        int numberOfOpenTag = 0;
        int numberOfClaseTag = 0;
        boolean validate = true;
        block9 : for (int i = 0; i < document.length(); ++i) {
            switch (document.charAt(i)) {
                case '\"': {
                    ++numberOfDoubleCommas;
                    continue block9;
                }
                case '(': {
                    ++numberOfOpenParentesis;
                    continue block9;
                }
                case ')': {
                    ++numberOfCloseParentesis;
                    continue block9;
                }
                case '{': {
                    ++numberOfOpenKey;
                    continue block9;
                }
                case '}': {
                    ++numberOfCloseKey;
                    continue block9;
                }
                case '<': {
                    ++numberOfOpenTag;
                    continue block9;
                }
                case '>': {
                    ++numberOfClaseTag;
                }
            }
        }
        if (validate && numberOfDoubleCommas % 2 != 0) {
            JOptionPane.showMessageDialog(mainForm.getMainPanel(), Internationalization.VALIDATION_ERROR_1, "", 2);
            validate = false;
        }
        if (validate && numberOfOpenTag != numberOfClaseTag) {
            JOptionPane.showMessageDialog(mainForm.getMainPanel(), Internationalization.VALIDATION_ERROR_2, "", 2);
            validate = false;
        }
        if (validate && numberOfCloseKey != numberOfOpenKey) {
            JOptionPane.showMessageDialog(mainForm.getMainPanel(), Internationalization.VALIDATION_ERROR_3, "", 2);
            validate = false;
        }
        if (validate && numberOfCloseParentesis != numberOfOpenParentesis) {
            JOptionPane.showMessageDialog(mainForm.getMainPanel(), Internationalization.VALIDATION_ERROR_4, "", 2);
            validate = false;
        }
        return validate;
    }

    public String writeIt(String s) throws IOException {
        String separator = System.getProperty("line.separator");
        String aux = s.replaceAll("\n", separator);
        return aux;
    }
}

