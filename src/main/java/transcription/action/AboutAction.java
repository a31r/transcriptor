/*
 * Decompiled with CFR 0_118.
 */
package transcription.action;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import transcription.gui.AcercaDeDialogTranspatent;
import transcription.gui.MainForm;

public class AboutAction
implements ActionListener {
    MainForm mainForm;

    public AboutAction(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    public MainForm getMainForm() {
        return this.mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        new AcercaDeDialogTranspatent(this.getMainForm().getMainPanel());
    }
}

