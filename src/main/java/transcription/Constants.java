/*
 * Decompiled with CFR 0_118.
 */
package transcription;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class Constants {
    public static String DIR_TEMPLATE = "." + File.pathSeparator + "resources" + File.pathSeparator + "templates";
    public static String FILE_HELP = "file:resources/help/index.html";
    public static String FILE_CONVERSION = ".\\conversionConfig.xml";
    public static String PERFORM_VALIDATION = "true";
    public static final String FILE_CONFIG = ".\\config.xml";
    public static int REWIND_SECOND_SEEK = 5;
    public static int FORWARD_SECOND_SEEK = 5;
    public static String TIME_STAMPING_LEVEL = "U";
    public static String ENCODING = "UTF-16";
    public static String FONT_FAMILY = "Arial";
    public static float RATIO_INCREASING = 0.1f;
    public static int FONT_SIZE = 14;
    public static final String APPLICATION_VERSION = "BACKBONE Transcriptor 2.2";
    public static String SAVE_FILE = null;
    public static final String ICON_ABOUT_CENTER = ".\\resources\\images\\acerca.png";
    public static final String ICON_NEW_TRANSCRIPTION = ".\\resources\\images\\new.gif";
    public static final String ICON_SAVE_TRANSCRIPTION = ".\\resources\\images\\save.gif";
    public static final String ICON_OPEN_TRANSCRIPTION = ".\\resources\\images\\open.gif";
    public static final String ICON_HELP = ".\\resources\\images\\help.gif";
    public static final String ICON_CONFIG = ".\\resources\\images\\config2.gif";
    public static final String ICON_ABOUT = ".\\resources\\images\\about.gif";
    public static final String ICON_LOAD_VIDEO = ".\\resources\\images\\loadinterview.gif";
    public static final String ICON_LOAD_VIDEO_URL = ".\\resources\\images\\loadurl.png";
    public static final String ICON_SET_START_TIME = ".\\resources\\images\\starttime.gif";
    public static final String ICON_SET_END_TIME = ".\\resources\\images\\endtime.gif";
    public static final String ICON_SET_INTERMEDIUM_TIME = ".\\resources\\images\\window_edit.png";
    public static final String ICON_MENU_CONFIG = ".\\resources\\images\\config.gif";
    public static final String ICON_MENU_NEW_TRANSCRIPTION = ".\\resources\\images\\new2.gif";
    public static final String ICON_MENU_SAVE_TRANSCRIPTION = ".\\resources\\images\\save2.gif";
    public static final String ICON_MENU_OPEN_TRANSCRIPTION = ".\\resources\\images\\open2.gif";
    public static final String ICON_MENU_LOAD_VIDEO = ".\\resources\\images\\loadinterview.gif";
    public static final String ICON_MENU_SET_START_TIME = ".\\resources\\images\\starttime.gif";
    public static final String ICON_MENU_SET_END_TIME = ".\\resources\\images\\endtime.gif";
    public static final String ICON_MENU_SET_INTERMEDIUM_TIME = ".\\resources\\images\\window_edit.png";
    public static final String ICON_MENU_HELP = ".\\resources\\images\\help2.gif";
    public static final String ICON_MENU_ABOUT = ".\\resources\\images\\about2.gif";
    public static final String SPLASH_ICON = ".\\resources\\images\\splash.png";

    public static void loadProperties() {
        Constants.loadProperties(".\\config.xml");
    }

    public static void loadProperties(String path) {
        try {
            Properties p = new Properties();
            p.loadFromXML(new FileInputStream(path));
            DIR_TEMPLATE = p.getProperty("DIR_TEMPLATE");
            ENCODING = p.getProperty("ENCODING");
            REWIND_SECOND_SEEK = Integer.parseInt(p.getProperty("REWIND_SECOND_SEEK"));
            FORWARD_SECOND_SEEK = Integer.parseInt(p.getProperty("FORWARD_SECOND_SEEK"));
            FONT_FAMILY = p.getProperty("FONT_FAMILY");
            FONT_SIZE = Integer.parseInt(p.getProperty("FONT_SIZE"));
            PERFORM_VALIDATION = p.getProperty("PERFORM_VALIDATION");
            TIME_STAMPING_LEVEL = p.getProperty("TIME_STAMPING_LEVEL");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveProperties(String path) {
        Properties p = new Properties();
        p.setProperty("DIR_TEMPLATE", DIR_TEMPLATE);
        p.setProperty("ENCODING", ENCODING);
        p.setProperty("REWIND_SECOND_SEEK", Integer.toString(REWIND_SECOND_SEEK));
        p.setProperty("FORWARD_SECOND_SEEK", Integer.toString(FORWARD_SECOND_SEEK));
        p.setProperty("FONT_FAMILY", FONT_FAMILY);
        p.setProperty("FONT_SIZE", Integer.toString(FONT_SIZE));
        p.setProperty("PERFORM_VALIDATION", PERFORM_VALIDATION);
        p.setProperty("TIME_STAMPING_LEVEL", TIME_STAMPING_LEVEL);
        try {
            p.storeToXML(new FileOutputStream(path, false), "Config File");
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}

