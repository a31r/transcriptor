/*
 * Decompiled with CFR 0_118.
 */
package transcription;

public class Internationalization {
    public static String OPEN_FILE_IOEXCEPTION = "Error transcription failed to open";
    public static String OPEN_FILE_ILLEGAL_ACCESS_EXCEPTION = "Error you don't have the required privileges to open this file";
    public static String OPEN_FILE_CLASS_NOT_FOUND_EXCEPTION = "The configuration file for inter-format conversions is not found";
    public static String NEW_FILE_FILE_NOT_FOUND_EXCEPTION = "The template file for transcriptions is not found";
    public static String NEW_FILE_IOEXCEPTION = "Error template failed to open";
    public static String HELP_IOEXCEPTION = "Error help file is not found";
    public static String SAVE_FILE_IOEXCEPTION = "Error in the saving process, the transcription has not been saved";
    public static String SET_END_TIME_EXCEPTION = "Please first set Start Time and then End Time";
    public static String SET_END_TIME_EXCEPTION_SEC = "Error each section must be placed between # symbols. Some # is missing";
    public static String CONVERSION_TXT = "I/O Error in the TXT conversion process (check the UTF-8 encoding)";
    public static String CONVERSION_RTF = "I/O Error in the RTF conversion process (check the UTF-8 encoding)";
    public static String START_TIME_ERROR = "Error this transcription need to start with the form: 'speaker:' ";
    public static String CONVERSION_RTF_BADLOCATION = "I/O Error in the RTF conversion process (check the UTF-8 encoding)";
    public static String VALIDATION_ERROR_1 = "Document Saved. But not valid, the number of \"double commmas (\")\" need to be odd.";
    public static String VALIDATION_ERROR_2 = "Document Saved. But not valid, the number of < simbol isn't the same of > simbol and need to be the same.";
    public static String VALIDATION_ERROR_3 = "Document Saved. But not valid, the number of open keys ('{') need to be the same of the close keys ('}').";
    public static String VALIDATION_ERROR_4 = "Document Saved. But not valid, the number of open parenthesis ('(') need to be the same of the close parenthesis (')').";
    public static String BUTTON_CLOSE = "Close";
    public static String BUTTON_ABOUT = "About...";
    public static String BUTTON_NEW = "New";
    public static String BUTTON_SAVE = "Save";
    public static String BUTTON_OPEN = "Open";
    public static String BUTTON_CONFIG = "Config";
    public static String CONFIG_TOOL_TIP = "Configuration of the application";
    public static String BUTTON_LOAD_VIDEO = "Load Video / Audio From File";
    public static String BUTTON_LOAD_VIDEO_URL = "Load Video / Audio From URL";
    public static String BUTTON_HELP = "Help";
    public static String SELECT_TEMPLATE = "Select template used on the transcription";
    public static String SELECT_TEMPLATE_TITLE = "Select template ...";
    public static String BUTTON_SET_START_TIME = "Set Start";
    public static String BUTTON_SET_END_TIME = "Set End";
    public static String BUTTON_SET_INTERMEDIUM_TIME = "Set Middle Timestamping";
    public static String STATUS_START = "Status: OK";
    public static String STATUS_OPEN = "Status: opened correctly";
    public static String STATUS_SAVE = "Status: saved correctly";
    public static String STATUS_NEW = "Status: created correctly";
    public static String MENU_TRANSCRIPTION = "Transcription";
    public static String MENU_INTERVIEW = "Video";
    public static String MENU_HELP = "Help";
    public static String LOAD_URL_MESSAGE = "Please, insert the URL Address of the multimedia file";
    public static String LOAD_URL_TITLE = "Please, insert the URL Address of the multimedia file";
    public static String TOOL_TIP_LOAD_URL = "Load Video through URL";
    public static String TOOL_TIP_LOAD_INTERVIEW = "Load Video through File";
    public static String TOOL_TIP_SET_START_TIME = "Set Start TimeStamp";
    public static String TOOL_TIP_SET_END_TIME = "Set End TimeStamp";
    public static String TOOL_TIP_SET_INTERMEDIUM_TIME = "Set Middle Timestamping";
    public static String TOOL_TIP_NEW_TRANSCRIPTION = "New Transcription";
    public static String TOOL_TIP_SAVE_TRANSCRIPTION = "Save Transcription";
    public static String TOOL_TIP_OPEN_TRANSCRIPTION = "Open Transcription";
    public static String TOOL_TIP_HELP = "Help";
    public static String TOOL_TIP_ABOUT = "About...";
    public static String TOOL_TIP_MEDIA_PLAYER = "Press CTRL + SPACE to shift between pause and play";
    public static String BOTTOM_FIRST_LINE_LABEL = "CTRL + C - Copy : CTRL + V - Paste : CTRL + Q - Save (Fast) : CTRL + E : Set Middle Time : CTRL + F : Forward. 5 sec.";
    public static String BOTTOM_SECOND_LINE_LABEL = "CTRL + SPACE - Play/Pause : CTRL + S - Set Start Time : CTRL + D : Set End Time : CTRL + R : Rew. 5 sec. :  CTRL + W Aspect R.";
}

