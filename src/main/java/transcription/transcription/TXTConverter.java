/*
 * Decompiled with CFR 0_118.
 */
package transcription.transcription;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import javax.swing.JOptionPane;
import transcription.Internationalization;
import transcription.transcription.TranscriptionConverter;

public class TXTConverter
implements TranscriptionConverter {
    @Override
    public String getTranscription(String path, String encoding) {
        String aux = null;
        String total = "";
        try {
            FileInputStream fin = new FileInputStream(path);
            BufferedReader br = new BufferedReader(new InputStreamReader((InputStream)fin, encoding));
            while ((aux = br.readLine()) != null) {
                total = total + aux + "\n";
            }
        }
        catch (IOException e) {
            JOptionPane.showMessageDialog(null, Internationalization.NEW_FILE_FILE_NOT_FOUND_EXCEPTION);
        }
        return total;
    }
}

