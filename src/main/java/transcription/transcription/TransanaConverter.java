/*
 * Decompiled with CFR 0_118.
 */
package transcription.transcription;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import javax.swing.JOptionPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.rtf.RTFEditorKit;
import transcription.Constants;
import transcription.Internationalization;
import transcription.transcription.TranscriptionConverter;

public class TransanaConverter
implements TranscriptionConverter {
    @Override
    public String getTranscription(String path, String encoding) {
        RTFEditorKit editor = new RTFEditorKit();
        DefaultStyledDocument doc = new DefaultStyledDocument();
        try {
            editor.read(new InputStreamReader((InputStream)new FileInputStream(path), encoding), (Document)doc, 0);
            String text = doc.getText(0, doc.getLength());
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < text.length(); ++i) {
                if (text.charAt(i) == '\u00a4') {
                    int j = i + 1;
                    while (text.charAt(j) != '>') {
                        ++j;
                    }
                    String time = text.substring(i + 2, j);
                    time = time.substring(0, time.length() - 1);
                    while (time.length() < 4) {
                        time = "0" + time;
                    }
                    sb.append("\n");
                    time = time.substring(0, time.length() - 2) + "," + time.substring(time.length() - 2);
                    sb.append("[" + time + "] ");
                    i = j;
                    continue;
                }
                if (text.charAt(i) == '<') {
                    char[] temp = new char[]{text.charAt(i + 1)};
                    String aux = new String(temp);
                    try {
                        Integer.parseInt(aux);
                        int j = i + 1;
                        while (text.charAt(j) != '>') {
                            ++j;
                        }
                        i = j;
                    }
                    catch (NumberFormatException nfe) {
                        sb.append(text.charAt(i));
                    }
                    continue;
                }
                sb.append(text.charAt(i));
            }
            FileReader fr = new FileReader(Constants.DIR_TEMPLATE);
            String aux = null;
            BufferedReader br = new BufferedReader(fr);
            String total = "";
            while ((aux = br.readLine()) != null) {
                total = total + aux + "\n";
            }
            total = total.substring(0, total.length() - 1);
            total = total.substring(0, total.lastIndexOf("\n"));
            total = total + sb.toString() + "[/BODY]";
            return total;
        }
        catch (IOException e) {
            JOptionPane.showMessageDialog(null, Internationalization.CONVERSION_RTF);
        }
        catch (BadLocationException e) {
            JOptionPane.showMessageDialog(null, Internationalization.CONVERSION_RTF_BADLOCATION);
        }
        return null;
    }
}

