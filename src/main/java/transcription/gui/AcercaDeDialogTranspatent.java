/*
 * Decompiled with CFR 0_118.
 */
package transcription.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import transcription.gui.TransparentBackground;

public class AcercaDeDialogTranspatent {
    public static boolean isOpaque = true;

    public AcercaDeDialogTranspatent(Component comp) {
        final JDialog jdAcercaDe = new JDialog();
        jdAcercaDe.setUndecorated(false);
        jdAcercaDe.setResizable(true);
        TransparentBackground bg = new TransparentBackground(jdAcercaDe);
        bg.setBorder(BorderFactory.createBevelBorder(0));
        BorderLayout b = new BorderLayout();
        bg.setLayout(b);
        JPanel jpNorte = new JPanel(new GridLayout(27, 1));
        JPanel jpCentro = new JPanel();
        JPanel jpSur = new JPanel(new FlowLayout());
        ImageIcon i1Imagen = new ImageIcon(".\\resources\\images\\backbone.png");
        JLabel jlImagen = new JLabel(i1Imagen, 0);
        ImageIcon i2Imagen = new ImageIcon(".\\resources\\images\\llp.png");
        JLabel j2Imagen = new JLabel(i2Imagen, 0);
        JButton jbOk = new JButton("Cerrar");
        jbOk.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                jdAcercaDe.setVisible(false);
            }
        });
        jpNorte.setOpaque(isOpaque);
        jpNorte.add(new JLabel(""));
        jpNorte.add(new JLabel("BACKBONE Transcriptor 2.2", 0));
        jpNorte.add(new JLabel(""));
        jpNorte.add(new JLabel("This freeware application developed for its use on BACKBONE: ", 0));
        jpNorte.add(new JLabel("Corpora for Content & Language Integrated Learning", 0));
        jpNorte.add(new JLabel(" ", 0));
        jpNorte.add(new JLabel(""));
        jpNorte.add(new JLabel("Jos\u00e9 Mar\u00eda Alcaraz: BACKBONE System Developer", 0));
        jpNorte.add(new JLabel("Email: jmalcaraz@gmail.com", 0));
        jpNorte.add(new JLabel(""));
        jpNorte.add(new JLabel("For more information visit http://purl.oclc.org/NET/backbone/site", 0));
        jpNorte.add(new JLabel("or e-mail us: pascualf@um.es and jmalcaraz@um.es", 0));
        jpNorte.add(new JLabel(""));
        jpNorte.add(new JLabel("Copyright (C) 2009 University Of Murcia", 0));
        jpNorte.add(new JLabel("", 0));
        jpNorte.add(new JLabel("This program is free software: you can redistribute it and/or modify", 0));
        jpNorte.add(new JLabel("it under the terms of the GNU General Public License as published by", 0));
        jpNorte.add(new JLabel("the Free Software Foundation, either version 3 of the License, or", 0));
        jpNorte.add(new JLabel("(at your option) any later version.", 0));
        jpNorte.add(new JLabel("", 0));
        jpNorte.add(new JLabel(" This program is distributed in the hope that it will be useful,", 0));
        jpNorte.add(new JLabel("but WITHOUT ANY WARRANTY; without even the implied warranty of", 0));
        jpNorte.add(new JLabel("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the", 0));
        jpNorte.add(new JLabel("GNU General Public License for more details.", 0));
        jpNorte.add(new JLabel("", 0));
        jpNorte.add(new JLabel("You should have received a copy of the GNU General Public License", 0));
        jpNorte.add(new JLabel("along with this program.  If not, see <http://www.gnu.org/licenses/>.", 0));
        jpCentro.setOpaque(isOpaque);
        jpCentro.add(jlImagen);
        jpCentro.add(j2Imagen);
        jpSur.setOpaque(isOpaque);
        jpSur.add(new JLabel(""));
        jpSur.add(jbOk);
        bg.add((Component)jpNorte, "North");
        bg.add((Component)jpCentro, "Center");
        bg.add((Component)jpSur, "South");
        jdAcercaDe.getContentPane().add(bg);
        jdAcercaDe.setTitle("About ...");
        jdAcercaDe.setSize(500, 600);
        jdAcercaDe.setLocationRelativeTo(comp);
        jdAcercaDe.setResizable(true);
        jdAcercaDe.setVisible(true);
    }

}

