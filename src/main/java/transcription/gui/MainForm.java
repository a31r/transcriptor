/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.stefankrause.xplookandfeel.XPLookAndFeel
 *  javax.media.bean.playerbean.MediaPlayer
 */
package transcription.gui;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import com.stefankrause.xplookandfeel.XPLookAndFeel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import javax.media.bean.playerbean.MediaPlayer;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import transcription.Constants;
import transcription.Internationalization;
import transcription.action.AboutAction;
import transcription.action.GuardarAction;
import transcription.action.HelpAction;
import transcription.action.IntegredWordKeyListener;
import transcription.action.MainFormSortKeyAction;
import transcription.action.NuevoAction;
import transcription.action.OpenAction;
import transcription.action.ShowConfigForm;
import transcription.action.mediaplayer.AssignInterviewFromFileAction;
import transcription.action.mediaplayer.AssignInterviewFromURLAction;
import transcription.action.mediaplayer.SetEndTimeAction;
import transcription.action.mediaplayer.SetIntermediumTimeAction;
import transcription.action.mediaplayer.SetStartTimeAction;
import transcription.action.utility.InsertTextAction;
import transcription.gui.SplashScreen;

public class MainForm {
    private JTextPane interviewLog;
    private JButton setStartButton;
    private JButton setEndButton;
    private JPanel mediaPanel;
    private JPanel mainPanel;
    private JButton assignInterviewButton;
    private JToolBar toolBar;
    private JButton setIntermidiumButton;
    private JLabel FristLineLabel;
    private JLabel seconfLineLabel;
    private JButton breakButton;
    private JButton cutButton;
    private JButton unclearButton;
    private JButton unclearUnButton;
    private JButton truncTrButton;
    private JButton alternativeAlButton;
    private JButton commentsButton;
    private JButton quotesButton;
    private JButton singleQuoteFilmsEtcButton;
    private JButton foreignFoButton;
    private JButton loadInteviewFromURLButton;
    private JLabel status;
    private JButton sectionBoundaryButton;
    private JButton sectionCommentsButton;
    private MediaPlayer mediaPlayer;
    private JMenuBar menuBar;

    public JLabel getStatus() {
        return this.status;
    }

    public void setMediaPlayer(MediaPlayer mediaPlayer) {
        this.mediaPlayer = mediaPlayer;
    }

    public JTextPane getInterviewLog() {
        return this.interviewLog;
    }

    public JButton getSetStartButton() {
        return this.setStartButton;
    }

    public JButton getSetEndButton() {
        return this.setEndButton;
    }

    public JPanel getMediaPanel() {
        return this.mediaPanel;
    }

    public JPanel getMainPanel() {
        return this.mainPanel;
    }

    public JButton getAssignInterviewButton() {
        return this.assignInterviewButton;
    }

    public MediaPlayer getMediaPlayer() {
        return this.mediaPlayer;
    }

    public JToolBar getToolBar() {
        return this.toolBar;
    }

    public JMenuBar getMenuBar() {
        return this.menuBar;
    }

    public JButton getSetIntermidiumButton() {
        return this.setIntermidiumButton;
    }

    public JButton getBreakButton() {
        return this.breakButton;
    }

    public JButton getCutButton() {
        return this.cutButton;
    }

    public JButton getUnclearButton() {
        return this.unclearButton;
    }

    public JButton getUnclearUnButton() {
        return this.unclearUnButton;
    }

    public JButton getTruncTrButton() {
        return this.truncTrButton;
    }

    public JButton getAlternativeAlButton() {
        return this.alternativeAlButton;
    }

    public JButton getCommentsButton() {
        return this.commentsButton;
    }

    public JButton getQuotesButton() {
        return this.quotesButton;
    }

    public JButton getSingleQuoteFilmsEtcButton() {
        return this.singleQuoteFilmsEtcButton;
    }

    public JButton getForeignFoButton() {
        return this.foreignFoButton;
    }

    public JButton getLoadInteviewFromURLButton() {
        return this.loadInteviewFromURLButton;
    }

    public JButton getSectionBoundaryButton() {
        return this.sectionBoundaryButton;
    }

    public MainForm() {
        this.$$$setupUI$$$();
        this.mediaPlayer = new MediaPlayer();
        this.mediaPlayer = new MediaPlayer();
        this.status.setForeground(Color.GREEN);
        this.getInterviewLog().setFont(new Font(Constants.FONT_FAMILY, 0, Constants.FONT_SIZE));
        this.getInterviewLog().addKeyListener(new IntegredWordKeyListener(this));
        this.getMediaPanel().add((Component)this.getMediaPlayer(), "Center");
        this.getMediaPlayer().setCachingControlVisible(true);
        this.getMediaPlayer().setControlPanelVisible(true);
        this.getMediaPlayer().setFixedAspectRatio(true);
        this.getMediaPlayer().setPopupActive(false);
        AssignInterviewFromFileAction loadInterviewListener = new AssignInterviewFromFileAction(this);
        this.getAssignInterviewButton().addActionListener(loadInterviewListener);
        this.getAssignInterviewButton().setToolTipText(Internationalization.TOOL_TIP_LOAD_INTERVIEW);
        AssignInterviewFromURLAction loadInterviewURL = new AssignInterviewFromURLAction(this);
        this.getLoadInteviewFromURLButton().addActionListener(loadInterviewURL);
        this.getLoadInteviewFromURLButton().setToolTipText(Internationalization.TOOL_TIP_LOAD_URL);
        SetStartTimeAction setStartListener = new SetStartTimeAction(this);
        this.getSetStartButton().addActionListener(setStartListener);
        this.getSetStartButton().setToolTipText(Internationalization.TOOL_TIP_SET_START_TIME);
        this.getSetStartButton().setEnabled(false);
        SetEndTimeAction setEndAction = new SetEndTimeAction(this);
        this.getSetEndButton().addActionListener(setEndAction);
        this.getSetEndButton().setToolTipText(Internationalization.TOOL_TIP_SET_END_TIME);
        this.getSetEndButton().setEnabled(false);
        SetIntermediumTimeAction setIntermediumAction = new SetIntermediumTimeAction(this);
        this.getSetIntermidiumButton().addActionListener(setIntermediumAction);
        this.getSetIntermidiumButton().setToolTipText(Internationalization.TOOL_TIP_SET_INTERMEDIUM_TIME);
        this.getSetIntermidiumButton().setEnabled(false);
        JButton nuevo = new JButton(new ImageIcon(".\\resources\\images\\new.gif"));
        nuevo.setToolTipText(Internationalization.TOOL_TIP_NEW_TRANSCRIPTION);
        NuevoAction nuevoListener = new NuevoAction(this);
        nuevo.addActionListener(nuevoListener);
        JButton guardar = new JButton(new ImageIcon(".\\resources\\images\\save.gif"));
        guardar.setToolTipText(Internationalization.TOOL_TIP_SAVE_TRANSCRIPTION);
        GuardarAction guardarListener = new GuardarAction(this);
        guardar.addActionListener(guardarListener);
        JButton open = new JButton(new ImageIcon(".\\resources\\images\\open.gif"));
        open.setToolTipText(Internationalization.TOOL_TIP_OPEN_TRANSCRIPTION);
        OpenAction openListener = new OpenAction(this);
        open.addActionListener(openListener);
        JButton help = new JButton(new ImageIcon(".\\resources\\images\\help.gif"));
        HelpAction helpListener = new HelpAction(this);
        help.setToolTipText(Internationalization.TOOL_TIP_HELP);
        help.addActionListener(helpListener);
        JButton about = new JButton(new ImageIcon(".\\resources\\images\\about.gif"));
        AboutAction aboutListener = new AboutAction(this);
        about.setToolTipText(Internationalization.TOOL_TIP_ABOUT);
        about.addActionListener(aboutListener);
        JButton config = new JButton(new ImageIcon(".\\resources\\images\\config2.gif"));
        ShowConfigForm configListener = new ShowConfigForm(this);
        config.setToolTipText(Internationalization.CONFIG_TOOL_TIP);
        config.addActionListener(configListener);
        this.getToolBar().add(nuevo);
        this.getToolBar().add(guardar);
        this.getToolBar().add(open);
        this.getToolBar().add(new JToolBar.Separator(new Dimension(30, 5)));
        this.getToolBar().add(config);
        this.getToolBar().add(new JToolBar.Separator(new Dimension(30, 5)));
        this.getToolBar().add(help);
        this.getToolBar().add(about);
        this.getToolBar().setFloatable(false);
        this.getToolBar().setRollover(true);
        this.getToolBar().setBorderPainted(true);
        this.getToolBar().setOpaque(false);
        this.menuBar = new JMenuBar();
        JMenu file = new JMenu(Internationalization.MENU_TRANSCRIPTION);
        JMenuItem fileNew = new JMenuItem(Internationalization.BUTTON_NEW, new ImageIcon(".\\resources\\images\\new2.gif"));
        fileNew.addActionListener(nuevoListener);
        JMenuItem fileSave = new JMenuItem(Internationalization.BUTTON_SAVE, new ImageIcon(".\\resources\\images\\save2.gif"));
        fileSave.addActionListener(guardarListener);
        JMenuItem fileOpen = new JMenuItem(Internationalization.BUTTON_OPEN, new ImageIcon(".\\resources\\images\\open2.gif"));
        fileOpen.addActionListener(openListener);
        file.add(fileNew);
        file.add(fileSave);
        file.add(fileOpen);
        this.getMenuBar().add(file);
        JMenu interview = new JMenu(Internationalization.MENU_INTERVIEW);
        JMenuItem loadInterview = new JMenuItem(Internationalization.BUTTON_LOAD_VIDEO, new ImageIcon(".\\resources\\images\\loadinterview.gif"));
        loadInterview.addActionListener(loadInterviewListener);
        JMenuItem loadInterviewURLb = new JMenuItem(Internationalization.BUTTON_LOAD_VIDEO_URL, new ImageIcon(".\\resources\\images\\loadurl.png"));
        loadInterviewURLb.addActionListener(loadInterviewURL);
        JMenuItem setStart = new JMenuItem(Internationalization.BUTTON_SET_START_TIME, new ImageIcon(".\\resources\\images\\starttime.gif"));
        setStart.addActionListener(setStartListener);
        setStart.setName("setStart");
        setStart.setEnabled(false);
        JMenuItem setEnd = new JMenuItem(Internationalization.BUTTON_SET_END_TIME, new ImageIcon(".\\resources\\images\\endtime.gif"));
        setEnd.addActionListener(setEndAction);
        setEnd.setName("setEnd");
        setEnd.setEnabled(false);
        JMenuItem setIntermidium = new JMenuItem(Internationalization.BUTTON_SET_INTERMEDIUM_TIME, new ImageIcon(".\\resources\\images\\window_edit.png"));
        setIntermidium.addActionListener(setEndAction);
        setIntermidium.setName("setIntermedium");
        setIntermidium.setEnabled(false);
        interview.add(loadInterview);
        interview.add(loadInterviewURLb);
        interview.add(setStart);
        interview.add(setIntermidium);
        interview.add(setEnd);
        this.getMenuBar().add(interview);
        JMenu configM = new JMenu(Internationalization.BUTTON_CONFIG);
        JMenuItem configItem = new JMenuItem(Internationalization.BUTTON_CONFIG, new ImageIcon(".\\resources\\images\\config.gif"));
        configItem.addActionListener(configListener);
        configM.add(configItem);
        this.getMenuBar().add(configM);
        JMenu helpMenu = new JMenu(Internationalization.MENU_HELP);
        JMenuItem helpItem = new JMenuItem(Internationalization.BUTTON_HELP, new ImageIcon(".\\resources\\images\\help2.gif"));
        helpItem.addActionListener(helpListener);
        JMenuItem aboutItem = new JMenuItem(Internationalization.BUTTON_ABOUT, new ImageIcon(".\\resources\\images\\about2.gif"));
        aboutItem.addActionListener(aboutListener);
        helpMenu.add(helpItem);
        helpMenu.add(aboutItem);
        this.getMenuBar().add(helpMenu);
        this.getInterviewLog().addKeyListener(new MainFormSortKeyAction(this));
        this.getMediaPanel().setToolTipText(Internationalization.TOOL_TIP_MEDIA_PLAYER);
        this.getBreakButton().addActionListener(new InsertTextAction(this, "<break/>"));
        this.getCutButton().addActionListener(new InsertTextAction(this, "<cut/>"));
        this.getUnclearButton().addActionListener(new InsertTextAction(this, "<unclear/>"));
        this.getUnclearUnButton().addActionListener(new InsertTextAction(this, "<unclear></unclear>"));
        this.getTruncTrButton().addActionListener(new InsertTextAction(this, "<trunc></trunc>"));
        this.getAlternativeAlButton().addActionListener(new InsertTextAction(this, "<alternative>spoken/formal form</alternative>"));
        this.getForeignFoButton().addActionListener(new InsertTextAction(this, "<foreign></foreign>"));
        this.getCommentsButton().addActionListener(new InsertTextAction(this, "( )"));
        this.getQuotesButton().addActionListener(new InsertTextAction(this, "\" \""));
        this.getSingleQuoteFilmsEtcButton().addActionListener(new InsertTextAction(this, "' '"));
        this.getSectionBoundaryButton().addActionListener(new InsertTextAction(this, "#"));
        this.sectionCommentsButton.addActionListener(new InsertTextAction(this, "{ }"));
        this.status.setText(Internationalization.STATUS_START);
    }

    public static void main(String[] args) {
        Constants.loadProperties();
        JPopupMenu.setDefaultLightWeightPopupEnabled(false);
        JFrame frame = new JFrame("BACKBONE Transcriptor 2.2");
        SplashScreen ss = new SplashScreen(new ImageIcon(".\\resources\\images\\splash.png"));
        ss.progressBar.setIndeterminate(true);
        ss.setLocationRelativeTo(frame);
        ss.setScreenVisible(true);
        frame.setDefaultCloseOperation(3);
        MainForm form = new MainForm();
        frame.setJMenuBar(form.getMenuBar());
        frame.setContentPane(form.getMainPanel());
        frame.setSize(1024, 748);
        frame.setState(0);
        frame.setExtendedState(6);
        frame.setVisible(true);
        ss.setScreenVisible(false);
    }

    private void createUIComponents() {
        try {
            UIManager.setLookAndFeel((LookAndFeel)new XPLookAndFeel());
        }
        catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        this.mainPanel = new JPanel();
        this.assignInterviewButton = new JButton(Internationalization.BUTTON_LOAD_VIDEO, new ImageIcon(".\\resources\\images\\loadinterview.gif"));
        this.setStartButton = new JButton(Internationalization.BUTTON_SET_START_TIME, new ImageIcon(".\\resources\\images\\starttime.gif"));
        this.setEndButton = new JButton(Internationalization.BUTTON_SET_END_TIME, new ImageIcon(".\\resources\\images\\endtime.gif"));
        this.setIntermidiumButton = new JButton(Internationalization.BUTTON_SET_INTERMEDIUM_TIME, new ImageIcon(".\\resources\\images\\window_edit.png"));
        this.FristLineLabel = new JLabel(Internationalization.BOTTOM_FIRST_LINE_LABEL, 0);
        this.seconfLineLabel = new JLabel(Internationalization.BOTTOM_SECOND_LINE_LABEL, 0);
        this.loadInteviewFromURLButton = new JButton(Internationalization.BUTTON_LOAD_VIDEO_URL, new ImageIcon(".\\resources\\images\\loadurl.png"));
    }

    private /* synthetic */ void $$$setupUI$$$() {
        JButton jButton;
        JButton jButton2;
        JButton jButton3;
        JToolBar jToolBar;
        JButton jButton4;
        JLabel jLabel;
        JButton jButton5;
        JButton jButton6;
        JButton jButton7;
        JTextPane jTextPane;
        JButton jButton8;
        JButton jButton9;
        JButton jButton10;
        JPanel jPanel;
        JButton jButton11;
        JButton jButton12;
        this.createUIComponents();
        JPanel jPanel2 = this.mainPanel;
        jPanel2.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1, false, false));
        JPanel jPanel3 = new JPanel();
        jPanel3.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel2.add((Component)jPanel3, new GridConstraints(1, 0, 1, 1, 0, 3, 3, 7, null, null, null));
        JPanel jPanel4 = new JPanel();
        jPanel4.setLayout(new GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel3.add((Component)jPanel4, new GridConstraints(1, 0, 1, 1, 0, 3, 3, 7, null, null, null));
        JPanel jPanel5 = new JPanel();
        jPanel5.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel4.add((Component)jPanel5, new GridConstraints(1, 0, 1, 1, 0, 3, 3, 1, null, null, null));
        JLabel jLabel2 = this.FristLineLabel;
        jLabel2.setText("Ctrl + C : Copy --- Ctrl + V : Paste --- Ctrl + Q : Save (Fast) --- Ctrl + I : Increasing Ratio --- Ctrl + U : Decreasing Raio --- Ctrl + E : Set Middle Time");
        jPanel5.add((Component)jLabel2, new GridConstraints(0, 0, 1, 1, 0, 0, 0, 0, null, null, null));
        JLabel jLabel3 = this.seconfLineLabel;
        jLabel3.setText("Ctrl + SPACE : Play/Pause --- Ctrl + S : Set Start Time --- Ctrl + D : Set End Time --- Ctrl + R : Rew. 5 sec. --- CTRL + F : Forward. 5 sec. ");
        jPanel5.add((Component)jLabel3, new GridConstraints(1, 0, 1, 1, 0, 0, 0, 0, null, null, null));
        JPanel jPanel6 = new JPanel();
        jPanel6.setLayout(new GridLayoutManager(13, 1, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel4.add((Component)jPanel6, new GridConstraints(0, 1, 1, 1, 0, 3, 3, 3, null, null, null));
        jPanel6.setBorder(BorderFactory.createTitledBorder(null, "Utilities", 0, 0, null, null));
        this.breakButton = jButton4 = new JButton();
        jButton4.setText("<break/>");
        jPanel6.add((Component)jButton4, new GridConstraints(0, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        Spacer spacer = new Spacer();
        jPanel6.add((Component)spacer, new GridConstraints(12, 0, 1, 1, 0, 2, 1, 6, null, null, null));
        this.cutButton = jButton8 = new JButton();
        jButton8.setText("<cut/>");
        jPanel6.add((Component)jButton8, new GridConstraints(1, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        this.unclearButton = jButton3 = new JButton();
        jButton3.setText("<unclear/>");
        jPanel6.add((Component)jButton3, new GridConstraints(2, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        this.unclearUnButton = jButton7 = new JButton();
        jButton7.setText("<unclear></un..>");
        jPanel6.add((Component)jButton7, new GridConstraints(3, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        this.truncTrButton = jButton11 = new JButton();
        jButton11.setText("<trunc></tr..>");
        jPanel6.add((Component)jButton11, new GridConstraints(4, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        this.alternativeAlButton = jButton2 = new JButton();
        jButton2.setText("<alternative></al..>");
        jPanel6.add((Component)jButton2, new GridConstraints(5, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        this.commentsButton = jButton9 = new JButton();
        jButton9.setText("comments ( )");
        jPanel6.add((Component)jButton9, new GridConstraints(7, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        this.quotesButton = jButton10 = new JButton();
        jButton10.setText("quotes \" \"");
        jPanel6.add((Component)jButton10, new GridConstraints(8, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        this.singleQuoteFilmsEtcButton = jButton5 = new JButton();
        jButton5.setText("single quote (films,etc.) ' '");
        jPanel6.add((Component)jButton5, new GridConstraints(9, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        this.foreignFoButton = jButton12 = new JButton();
        jButton12.setText("<foreign></fo..>");
        jPanel6.add((Component)jButton12, new GridConstraints(6, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        this.sectionBoundaryButton = jButton = new JButton();
        jButton.setText("section boundary #");
        jPanel6.add((Component)jButton, new GridConstraints(10, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        this.sectionCommentsButton = jButton6 = new JButton();
        jButton6.setText("section comments { }");
        jPanel6.add((Component)jButton6, new GridConstraints(11, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        JPanel jPanel7 = new JPanel();
        jPanel7.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel4.add((Component)jPanel7, new GridConstraints(1, 1, 1, 1, 0, 3, 1, 1, null, null, null));
        this.status = jLabel = new JLabel();
        jLabel.setText("Label");
        jPanel7.add((Component)jLabel, new GridConstraints(0, 0, 1, 1, 8, 0, 0, 0, null, null, null));
        JPanel jPanel8 = new JPanel();
        jPanel8.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel4.add((Component)jPanel8, new GridConstraints(0, 0, 1, 1, 0, 3, 7, 7, null, new Dimension(321, 270), null));
        jPanel8.setBorder(BorderFactory.createTitledBorder(null, "Media Player", 0, 0, null, null));
        this.mediaPanel = jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout(0, 0));
        jPanel.setEnabled(false);
        jPanel8.add((Component)jPanel, new GridConstraints(0, 1, 1, 1, 0, 3, 7, 7, null, null, null, 0, true));
        jPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), null, 0, 0, null, null));
        JPanel jPanel9 = new JPanel();
        jPanel9.setLayout(new GridLayoutManager(5, 1, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel8.add((Component)jPanel9, new GridConstraints(0, 0, 1, 1, 9, 0, 3, 3, null, new Dimension(33, 59), null));
        JButton jButton13 = this.assignInterviewButton;
        jButton13.setText("Load Video From File");
        jPanel9.add((Component)jButton13, new GridConstraints(0, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        JButton jButton14 = this.loadInteviewFromURLButton;
        jButton14.setText("Load Video From URL");
        jPanel9.add((Component)jButton14, new GridConstraints(1, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        JButton jButton15 = this.setStartButton;
        jButton15.setText("Set Start Time");
        jPanel9.add((Component)jButton15, new GridConstraints(2, 0, 1, 1, 0, 1, 3, 0, null, new Dimension(86, 25), null));
        JButton jButton16 = this.setEndButton;
        jButton16.setText("Set End Time");
        jPanel9.add((Component)jButton16, new GridConstraints(3, 0, 1, 1, 0, 1, 3, 0, null, new Dimension(90, 25), null));
        JButton jButton17 = this.setIntermidiumButton;
        jButton17.setText("Set Middle Time");
        jPanel9.add((Component)jButton17, new GridConstraints(4, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        JPanel jPanel10 = new JPanel();
        jPanel10.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel3.add((Component)jPanel10, new GridConstraints(0, 0, 1, 1, 0, 3, 3, 7, null, null, null));
        jPanel10.setBorder(BorderFactory.createTitledBorder(null, "Transcription", 0, 0, null, null));
        JScrollPane jScrollPane = new JScrollPane();
        jScrollPane.setVerticalScrollBarPolicy(22);
        jScrollPane.setHorizontalScrollBarPolicy(32);
        jPanel10.add((Component)jScrollPane, new GridConstraints(0, 0, 1, 1, 0, 3, 7, 3, null, new Dimension(445, 238), null));
        this.interviewLog = jTextPane = new JTextPane();
        jTextPane.setEditable(true);
        jTextPane.setEnabled(true);
        jScrollPane.setViewportView(jTextPane);
        this.toolBar = jToolBar = new JToolBar();
        jPanel2.add((Component)jToolBar, new GridConstraints(0, 0, 1, 1, 0, 1, 6, 0, new Dimension(-1, 50), new Dimension(-1, 50), null));
    }

    public /* synthetic */ JComponent $$$getRootComponent$$$() {
        return this.mainPanel;
    }
}

