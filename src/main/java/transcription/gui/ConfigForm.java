/*
 * Decompiled with CFR 0_118.
 */
package transcription.gui;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.border.Border;
import transcription.Constants;
import transcription.gui.CustomFontChooserDialog;
import transcription.gui.MainForm;

public class ConfigForm {
    private JPanel panelPrincipal;
    private JButton fontConfigButton;
    private JSlider rewindSlider;
    private JSlider forwardSlider;
    private JButton saveChangesButton;
    private JButton selectDefaultTemplateFileButton;
    private JCheckBox validateCheckBox;
    private JRadioButton sectionRadioButton;
    private JRadioButton utteranceRadioButton;
    CustomFontChooserDialog fontDialog;
    MainForm mainForm;

    public ConfigForm(final MainForm mainForm) {
        this.mainForm = mainForm;
        this.$$$setupUI$$$();
        final JDialog dialog = new JDialog((Frame)mainForm.getMainPanel().getTopLevelAncestor(), true);
        dialog.setContentPane(this.panelPrincipal);
        this.fontDialog = new CustomFontChooserDialog((Frame)mainForm.getMainPanel().getTopLevelAncestor());
        this.fontDialog.setLocationRelativeTo(mainForm.getMainPanel());
        this.fontConfigButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                ConfigForm.this.fontDialog.setVisible(true);
            }
        });
        this.rewindSlider.setMinorTickSpacing(1);
        this.rewindSlider.setMajorTickSpacing(5);
        this.rewindSlider.setMinimum(1);
        this.rewindSlider.setMaximum(30);
        this.rewindSlider.setPaintTicks(true);
        this.rewindSlider.setPaintTrack(true);
        this.rewindSlider.setPaintLabels(true);
        this.rewindSlider.setValue(Constants.REWIND_SECOND_SEEK);
        this.forwardSlider.setMinorTickSpacing(1);
        this.forwardSlider.setMajorTickSpacing(5);
        this.forwardSlider.setMinimum(1);
        this.forwardSlider.setMaximum(30);
        this.forwardSlider.setPaintTicks(true);
        this.forwardSlider.setPaintTrack(true);
        this.forwardSlider.setPaintLabels(true);
        this.forwardSlider.setValue(Constants.FORWARD_SECOND_SEEK);
        if (Constants.PERFORM_VALIDATION.equalsIgnoreCase("TRUE")) {
            this.validateCheckBox.setSelected(true);
        } else {
            this.validateCheckBox.setSelected(false);
        }
        if (Constants.TIME_STAMPING_LEVEL.equalsIgnoreCase("U")) {
            this.utteranceRadioButton.setSelected(true);
        } else {
            this.sectionRadioButton.setSelected(true);
        }
        this.saveChangesButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Constants.REWIND_SECOND_SEEK = ConfigForm.this.rewindSlider.getValue();
                Constants.FORWARD_SECOND_SEEK = ConfigForm.this.forwardSlider.getValue();
                Constants.PERFORM_VALIDATION = ConfigForm.this.validateCheckBox.isSelected() ? "TRUE" : "FALSE";
                Constants.TIME_STAMPING_LEVEL = ConfigForm.this.utteranceRadioButton.isSelected() ? "U" : "S";
                Constants.saveProperties(".\\config.xml");
                dialog.setVisible(false);
            }
        });
        this.selectDefaultTemplateFileButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser chooser = new JFileChooser();
                chooser.setFileSelectionMode(1);
                if (chooser.showOpenDialog(mainForm.getMainPanel()) == 0) {
                    File selected = chooser.getSelectedFile();
                    Constants.DIR_TEMPLATE = selected.getAbsolutePath();
                }
            }
        });
        dialog.pack();
        dialog.setLocationRelativeTo(mainForm.getMainPanel());
        dialog.setVisible(true);
    }

    private /* synthetic */ void $$$setupUI$$$() {
        JSlider jSlider;
        JButton jButton;
        JSlider jSlider2;
        JButton jButton2;
        JRadioButton jRadioButton;
        JCheckBox jCheckBox;
        JPanel jPanel;
        JRadioButton jRadioButton2;
        JButton jButton3;
        this.panelPrincipal = jPanel = new JPanel();
        jPanel.setLayout(new GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1, false, false));
        JPanel jPanel2 = new JPanel();
        jPanel2.setLayout(new GridLayoutManager(3, 2, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel.add((Component)jPanel2, new GridConstraints(0, 0, 1, 1, 0, 3, 3, 3, null, null, null));
        JPanel jPanel3 = new JPanel();
        jPanel3.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel2.add((Component)jPanel3, new GridConstraints(0, 0, 1, 1, 0, 3, 3, 3, null, null, null));
        jPanel3.setBorder(BorderFactory.createTitledBorder(null, "Configure Font", 0, 0, null, null));
        this.fontConfigButton = jButton = new JButton();
        jButton.setText("Font Config");
        jPanel3.add((Component)jButton, new GridConstraints(0, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        JPanel jPanel4 = new JPanel();
        jPanel4.setLayout(new GridLayoutManager(4, 3, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel2.add((Component)jPanel4, new GridConstraints(2, 0, 1, 2, 0, 3, 3, 3, null, null, null));
        jPanel4.setBorder(BorderFactory.createTitledBorder(null, "Configure Behaviour", 0, 0, null, null));
        this.rewindSlider = jSlider2 = new JSlider();
        jPanel4.add((Component)jSlider2, new GridConstraints(0, 0, 1, 2, 8, 1, 6, 0, null, null, null));
        JLabel jLabel = new JLabel();
        jLabel.setText("Number of Rewind Seek (sec.)");
        jPanel4.add((Component)jLabel, new GridConstraints(0, 2, 1, 1, 8, 0, 0, 0, null, null, null));
        this.forwardSlider = jSlider = new JSlider();
        jPanel4.add((Component)jSlider, new GridConstraints(1, 0, 1, 2, 8, 1, 6, 0, null, null, null));
        JLabel jLabel2 = new JLabel();
        jLabel2.setText("Number of Forward Seek (sec.)");
        jPanel4.add((Component)jLabel2, new GridConstraints(1, 2, 1, 1, 8, 0, 0, 0, null, null, null));
        this.validateCheckBox = jCheckBox = new JCheckBox();
        jCheckBox.setText("Validate");
        jPanel4.add((Component)jCheckBox, new GridConstraints(2, 0, 1, 2, 8, 0, 3, 0, null, null, null));
        JLabel jLabel3 = new JLabel();
        jLabel3.setText("Perform Document Validation on save ");
        jPanel4.add((Component)jLabel3, new GridConstraints(2, 2, 1, 1, 8, 0, 0, 0, null, null, null));
        this.sectionRadioButton = jRadioButton = new JRadioButton();
        jRadioButton.setText("Section");
        jPanel4.add((Component)jRadioButton, new GridConstraints(3, 0, 1, 1, 8, 0, 3, 0, null, null, null));
        this.utteranceRadioButton = jRadioButton2 = new JRadioButton();
        jRadioButton2.setText("Utterance");
        jPanel4.add((Component)jRadioButton2, new GridConstraints(3, 1, 1, 1, 8, 0, 3, 0, null, null, null));
        JLabel jLabel4 = new JLabel();
        jLabel4.setText("Time Stamping Level");
        jPanel4.add((Component)jLabel4, new GridConstraints(3, 2, 1, 1, 8, 0, 0, 0, null, null, null));
        JPanel jPanel5 = new JPanel();
        jPanel5.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel2.add((Component)jPanel5, new GridConstraints(0, 1, 2, 1, 0, 3, 3, 3, null, null, null));
        jPanel5.setBorder(BorderFactory.createTitledBorder(null, "Save Changes", 0, 0, null, null));
        this.saveChangesButton = jButton2 = new JButton();
        jButton2.setText("Save Changes");
        jPanel5.add((Component)jButton2, new GridConstraints(0, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        JPanel jPanel6 = new JPanel();
        jPanel6.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel2.add((Component)jPanel6, new GridConstraints(1, 0, 1, 1, 0, 3, 3, 3, null, null, null));
        jPanel6.setBorder(BorderFactory.createTitledBorder(null, "Configure transcription Template", 0, 0, null, null));
        this.selectDefaultTemplateFileButton = jButton3 = new JButton();
        jButton3.setText("Select Template Directory");
        jPanel6.add((Component)jButton3, new GridConstraints(0, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        Spacer spacer = new Spacer();
        jPanel.add((Component)spacer, new GridConstraints(0, 1, 1, 1, 0, 1, 6, 1, null, null, null));
        Spacer spacer2 = new Spacer();
        jPanel.add((Component)spacer2, new GridConstraints(1, 0, 1, 1, 0, 2, 1, 6, null, null, null));
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(jRadioButton);
        buttonGroup.add(jRadioButton2);
    }

    public /* synthetic */ JComponent $$$getRootComponent$$$() {
        return this.panelPrincipal;
    }

}

