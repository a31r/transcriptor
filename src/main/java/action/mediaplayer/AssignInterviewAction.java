/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  javax.media.bean.playerbean.MediaPlayer
 */
package action.mediaplayer;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.media.bean.playerbean.MediaPlayer;
import javax.swing.JFileChooser;

public class AssignInterviewAction
implements ActionListener {
    MediaPlayer mediaPlayer;

    public MediaPlayer getMediaPlayer() {
        return this.mediaPlayer;
    }

    public void setMediaPlayer(MediaPlayer mediaPlayer) {
        this.mediaPlayer = mediaPlayer;
    }

    public AssignInterviewAction(MediaPlayer mediaPlayer) {
        this.mediaPlayer = mediaPlayer;
    }

    public void actionPerformed(ActionEvent e) {
        JFileChooser chooser = new JFileChooser();
        if (0 == chooser.showOpenDialog((Component)this.getMediaPlayer())) {
            String path = chooser.getSelectedFile().getAbsolutePath();
            this.getMediaPlayer().setMediaLocation("file:///" + path);
            this.getMediaPlayer().start();
        }
    }
}

