/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  javax.media.Time
 *  javax.media.bean.playerbean.MediaPlayer
 */
package action.mediaplayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import javax.media.Time;
import javax.media.bean.playerbean.MediaPlayer;

public class SetStartTimeAction
implements ActionListener {
    MediaPlayer mediaPlayer;

    public SetStartTimeAction(MediaPlayer mediaPlayer) {
        this.mediaPlayer = mediaPlayer;
    }

    public MediaPlayer getMediaPlayer() {
        return this.mediaPlayer;
    }

    public void setMediaPlayer(MediaPlayer mediaPlayer) {
        this.mediaPlayer = mediaPlayer;
    }

    public void actionPerformed(ActionEvent e) {
        System.out.println(this.getMediaPlayer().getMediaTime().getSeconds());
    }
}

