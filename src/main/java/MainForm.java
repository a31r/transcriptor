/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  javax.media.bean.playerbean.MediaPlayer
 */
import action.mediaplayer.AssignInterviewAction;
import action.mediaplayer.SetStartTimeAction;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import javax.media.bean.playerbean.MediaPlayer;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.border.Border;

public class MainForm {
    private JTree InterviewManager;
    private JEditorPane interviewLog;
    private JList SpeakerList;
    private JButton addSpeaker;
    private JButton setStartButton;
    private JButton setEndButton;
    private JPanel mediaPanel;
    private JPanel mainPanel;
    private JButton assignInterviewButton;
    private JTextArea transcriptionField;
    private MediaPlayer mediaPlayer = new MediaPlayer();

    public JTree getInterviewManager() {
        return this.InterviewManager;
    }

    public void setInterviewManager(JTree interviewManager) {
        this.InterviewManager = interviewManager;
    }

    public JEditorPane getInterviewLog() {
        return this.interviewLog;
    }

    public void setInterviewLog(JEditorPane interviewLog) {
        this.interviewLog = interviewLog;
    }

    public JList getSpeakerList() {
        return this.SpeakerList;
    }

    public void setSpeakerList(JList speakerList) {
        this.SpeakerList = speakerList;
    }

    public JButton getAddSpeaker() {
        return this.addSpeaker;
    }

    public void setAddSpeaker(JButton addSpeaker) {
        this.addSpeaker = addSpeaker;
    }

    public JButton getSetStartButton() {
        return this.setStartButton;
    }

    public void setSetStartButton(JButton setStartButton) {
        this.setStartButton = setStartButton;
    }

    public JButton getSetEndButton() {
        return this.setEndButton;
    }

    public void setSetEndButton(JButton setEndButton) {
        this.setEndButton = setEndButton;
    }

    public JPanel getMediaPanel() {
        return this.mediaPanel;
    }

    public void setMediaPanel(JPanel mediaPanel) {
        this.mediaPanel = mediaPanel;
    }

    public JPanel getMainPanel() {
        return this.mainPanel;
    }

    public void setMainPanel(JPanel mainPanel) {
        this.mainPanel = mainPanel;
    }

    public JButton getAssignInterviewButton() {
        return this.assignInterviewButton;
    }

    public void setAssignInterviewButton(JButton assignInterviewButton) {
        this.assignInterviewButton = assignInterviewButton;
    }

    public MediaPlayer getMediaPlayer() {
        return this.mediaPlayer;
    }

    public void setMediaPlayer(MediaPlayer mediaPlayer) {
        this.mediaPlayer = mediaPlayer;
    }

    public MainForm() {
        this.$$$setupUI$$$();
        this.mediaPlayer = new MediaPlayer();
        this.getMediaPanel().add((Component)this.getMediaPlayer(), "Center");
        this.getAssignInterviewButton().addActionListener(new AssignInterviewAction(this.getMediaPlayer()));
        this.getSetStartButton().addActionListener(new SetStartTimeAction(this.getMediaPlayer()));
        this.getMediaPlayer().setControlPanelVisible(true);
        this.getMediaPlayer().setFixedAspectRatio(true);
        this.getMediaPlayer().setPopupActive(false);
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Transcriptor 1.0");
        MainForm form = new MainForm();
        frame.setContentPane(form.getMainPanel());
        frame.setSize(1024, 748);
        frame.setVisible(true);
    }

    private void createUIComponents() {
    }

    private void $$$setupUI$$$() {
        this.mainPanel = new JPanel();
        this.mainPanel.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1));
        this.mainPanel.add((Component)panel1, new GridConstraints(1, 0, 1, 1, 0, 3, 3, 3, null, null, null, 0, false));
        JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel1.add((Component)panel2, new GridConstraints(0, 0, 2, 1, 0, 3, 3, 3, null, null, null, 0, false));
        panel2.setBorder(BorderFactory.createTitledBorder("Corpus"));
        JScrollPane scrollPane1 = new JScrollPane();
        scrollPane1.setHorizontalScrollBarPolicy(32);
        scrollPane1.setVerticalScrollBarPolicy(22);
        panel2.add((Component)scrollPane1, new GridConstraints(0, 0, 1, 1, 0, 3, 5, 5, null, new Dimension(158, 335), null, 0, false));
        this.InterviewManager = new JTree();
        this.InterviewManager.setBackground(SystemColor.control);
        this.InterviewManager.setEditable(false);
        scrollPane1.setViewportView(this.InterviewManager);
        JPanel panel3 = new JPanel();
        panel3.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel1.add((Component)panel3, new GridConstraints(1, 1, 1, 1, 0, 3, 3, 5, null, null, null, 0, false));
        panel3.setBorder(BorderFactory.createTitledBorder("Interview"));
        JScrollPane scrollPane2 = new JScrollPane();
        scrollPane2.setHorizontalScrollBarPolicy(32);
        scrollPane2.setVerticalScrollBarPolicy(22);
        panel3.add((Component)scrollPane2, new GridConstraints(0, 0, 1, 1, 0, 3, 5, 3, null, new Dimension(445, 151), null, 0, false));
        this.interviewLog = new JEditorPane();
        this.interviewLog.setEditable(false);
        this.interviewLog.setEnabled(false);
        scrollPane2.setViewportView(this.interviewLog);
        JPanel panel4 = new JPanel();
        panel4.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        panel1.add((Component)panel4, new GridConstraints(0, 1, 1, 1, 0, 3, 3, 5, null, null, null, 0, false));
        JPanel panel5 = new JPanel();
        panel5.setLayout(new GridLayoutManager(2, 3, new Insets(0, 0, 0, 0), -1, -1));
        panel4.add((Component)panel5, new GridConstraints(0, 1, 1, 1, 0, 3, 5, 5, null, null, null, 0, false));
        panel5.setBorder(BorderFactory.createTitledBorder("Media Player"));
        this.setStartButton = new JButton();
        this.setStartButton.setText("Set Start");
        panel5.add((Component)this.setStartButton, new GridConstraints(1, 1, 1, 1, 0, 1, 3, 0, null, new Dimension(86, 25), null, 0, false));
        this.setEndButton = new JButton();
        this.setEndButton.setText("Set End");
        panel5.add((Component)this.setEndButton, new GridConstraints(1, 2, 1, 1, 0, 1, 3, 0, null, new Dimension(90, 25), null, 0, false));
        this.mediaPanel = new JPanel();
        this.mediaPanel.setLayout(new BorderLayout(0, 0));
        panel5.add((Component)this.mediaPanel, new GridConstraints(0, 0, 1, 3, 0, 3, 5, 5, null, null, null, 0, false));
        this.assignInterviewButton = new JButton();
        this.assignInterviewButton.setText("Assign Interview");
        panel5.add((Component)this.assignInterviewButton, new GridConstraints(1, 0, 1, 1, 0, 1, 3, 0, null, null, null, 0, false));
        JPanel panel6 = new JPanel();
        panel6.setLayout(new GridLayoutManager(3, 3, new Insets(0, 0, 0, 0), -1, -1));
        panel4.add((Component)panel6, new GridConstraints(0, 0, 1, 1, 0, 3, 3, 3, null, null, null, 0, false));
        panel6.setBorder(BorderFactory.createTitledBorder("Speaker"));
        this.addSpeaker = new JButton();
        this.addSpeaker.setText("Add Speaker");
        panel6.add((Component)this.addSpeaker, new GridConstraints(1, 1, 1, 1, 0, 1, 0, 0, null, new Dimension(45, 25), null, 0, false));
        Spacer spacer1 = new Spacer();
        panel6.add((Component)spacer1, new GridConstraints(1, 2, 1, 1, 0, 1, 4, 1, null, new Dimension(26, 11), null, 0, false));
        Spacer spacer2 = new Spacer();
        panel6.add((Component)spacer2, new GridConstraints(1, 0, 1, 1, 0, 1, 4, 1, null, new Dimension(13, 11), null, 0, false));
        JScrollPane scrollPane3 = new JScrollPane();
        scrollPane3.setVerticalScrollBarPolicy(22);
        panel6.add((Component)scrollPane3, new GridConstraints(0, 0, 1, 3, 0, 3, 5, 3, null, null, null, 0, false));
        this.SpeakerList = new JList();
        this.SpeakerList.setSelectionForeground(SystemColor.control);
        scrollPane3.setViewportView(this.SpeakerList);
        JPanel panel7 = new JPanel();
        panel7.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel6.add((Component)panel7, new GridConstraints(2, 0, 1, 3, 0, 3, 5, 5, null, null, null, 0, false));
        JScrollPane scrollPane4 = new JScrollPane();
        scrollPane4.setHorizontalScrollBarPolicy(32);
        scrollPane4.setVerticalScrollBarPolicy(22);
        panel7.add((Component)scrollPane4, new GridConstraints(0, 0, 1, 1, 0, 3, 5, 5, null, null, null, 0, false));
        this.transcriptionField = new JTextArea();
        scrollPane4.setViewportView(this.transcriptionField);
        JToolBar toolBar1 = new JToolBar();
        this.mainPanel.add((Component)toolBar1, new GridConstraints(0, 0, 1, 1, 0, 1, 4, 0, null, new Dimension(-1, 20), null, 0, false));
    }

    public JComponent $$$getRootComponent$$$() {
        return this.mainPanel;
    }
}

